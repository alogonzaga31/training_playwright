const { chromium } = require('playwright');

(async () => {
  const browser = await chromium.launch({
    headless: false
  });
  const context = await browser.newContext();

  // Open new page
  const page = await context.newPage();

  // Go to https://javan.co.id/
  await page.goto('https://javan.co.id/');

  // Click #mobileMenu >> text=Career
  await page.locator('#mobileMenu >> text=Career').click();
  // assert.equal(page.url(), 'https://javan.co.id/career');

  // Click text=Programmer PHP Apply Job >> img[alt="job"]
  await page.locator('text=Programmer PHP Apply Job >> img[alt="job"]').click();
  // assert.equal(page.url(), 'https://javan.co.id/career/programmer-php/6');

  // Click #mobileMenu >> text=Data Analytics
  await page.locator('#mobileMenu >> text=Data Analytics').click();
  // assert.equal(page.url(), 'https://javan.co.id/product/analytics');

  // Click text=Analyze My Business Data
  await page.locator('text=Analyze My Business Data').click();
  // assert.equal(page.url(), 'https://javan.co.id/contact');

  // ---------------------
  await context.close();
  await browser.close();
})();